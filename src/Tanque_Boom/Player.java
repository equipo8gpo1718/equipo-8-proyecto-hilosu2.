/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tanque_Boom;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;



/**
 *
 * @author Alexis
 */
public class Player extends JFrame{ 
    private int Xrueda1,Yrueda1;   //aqui como en los demas "X" y "Y" declaramos las variables que usara como coordenadas cada parte del tanque jugador 
    private int Xrueda2,Yrueda2;
    private int Xbase,Ybase;
    private int Xcabeza,Ycabeza;
    private int Xdisparador,Ydisparador; 
   //private Color color;   //variable color para designar los colores de las figuras dibujadas
    
    public Player()   //constructor
    {
        setTitle("LEVEL 1");    //titulo del frame
        setSize(1050,700);    //tamaño del frame
        setLocationRelativeTo(null);    //ubicacion
        setBackground(Color.WHITE);  //cambio del fondo 
        //color = Color.BLUE;    //iniciamos la variable color 
        Xrueda1 = 30;    //iniciamos variables de X y Y
        Yrueda1 = 450;      
        Xrueda2 = 80;
        Yrueda2=450;
        Xbase=20;
        Ybase = 433;
        Xcabeza = 30;
        Ycabeza = 413;
        Xdisparador = 70;
        Ydisparador = 404;
        movimientosPlayer();   //metodo que almacena todas las figuras y movimientos que esta efectua 
        setVisible(true);  //hacemos visible el frame 
    }
    
    public void paint(Graphics draw)   //clase Graphics para dibujar 
    {
        draw.setColor(Color.BLACK);      //pintamos con negro las ruedas del tanque.... el dibujo va de abajo hacia arriba
        draw.fillOval(Xrueda1, Yrueda1, 20, 20);
        draw.fillOval(Xrueda2, Yrueda2, 20, 20);
        if(Color.BLACK == Color.BLACK)             //hacemos un condicional para poder usar graphics2d y no afecte todo el metodo paint()
        {                                                  //ya que todo se dibujaria en 2D
           // draw.setColor(Color.BLUE);
             Graphics2D cuerpo = (Graphics2D) draw;       //cast para cambiar a graficos 2D
             cuerpo.setPaint(new GradientPaint(10,20, Color.GREEN, 35,70, Color.DARK_GRAY, true));           //usamos un gradiente y lo dibujamos en 
             cuerpo.fill(new Rectangle2D.Double(Xbase,Ybase,90,20));                                         //un rectangulo
             cuerpo.setPaint(new GradientPaint(15,10, Color.GREEN, 45,60, Color.DARK_GRAY, true));        //de nuevo otro gradiente para otro rectangulo
             cuerpo.fill(new Rectangle2D.Double(Xcabeza,Ycabeza,70,20)); 
       // draw.fillRect(Xbase, Ybase, 90, 20);
        //draw.fillRect(Xcabeza,Ycabeza,70, 20);
        }
        if (Color.BLACK == Color.BLACK)    //otro condicional 
        {
            Graphics2D tirador = (Graphics2D) draw;    //de nuevo un cast
            tirador.setPaint(new GradientPaint(15,10, Color.RED, 35,60, Color.DARK_GRAY, true));         //rectangulo pequeño que simula un cañon del tanque  
            tirador.fill(new Rectangle2D.Double(Xdisparador,Ydisparador,60,10)); 
            //draw.setColor(Color.RED);
           // draw.fillRect(Xdisparador, Ydisparador, 60, 10);
        }
        
    }
    
     public void movimientosPlayer(){ //movemos toda la figura
           addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e){
                int control = e.getKeyCode();
                switch(control){
                    case KeyEvent.VK_UP:                        
                        Yrueda1 -=5;
                        Yrueda2 -=5;
                        Ybase -=6;
                        Ycabeza -=7;
                        Ydisparador -=8;
                        repaint();
                        break;
                    case KeyEvent.VK_DOWN:
                        Yrueda1+=5;
                        Yrueda2 +=5;
                        Ybase +=6;
                        Ycabeza +=7;
                        Ydisparador +=8;
                        repaint();
                        break;
                }
               repaint();
            }
        });
     }
}
