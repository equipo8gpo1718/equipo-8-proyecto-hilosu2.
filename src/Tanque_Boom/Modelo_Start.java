/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tanque_Boom;

/**
 *
 * @author Alexis
 */
public class Modelo_Start {
    public int puntuacion, vida; 
    
    public int getPuntuacion()
    {
        return puntuacion;
    }
    
    public int getVida()
    {
        return vida; 
    }
    
    public void setPuntuacion(int puntuacion)
    {
        this.puntuacion = puntuacion; 
    }
    
    public void setVida(int vida)
    {
        this.vida = vida; 
    }
}
