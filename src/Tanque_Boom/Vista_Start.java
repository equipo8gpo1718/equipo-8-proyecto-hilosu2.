/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tanque_Boom;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Alexis
 */
public class Vista_Start extends JFrame{
    public JPanel panelContenedor, panelJugador, panelLevels, panelBotones, panelLabels; 
    public JButton level1, level2, level3, exit, start;
    public JLabel puntuacion, vida,tanque;
    private GridBagConstraints gbc;
    //ImageIcon tank = new ImageIcon(); 
    
    public Vista_Start()
    {
       /* Creamos solo la vista de todo el frame principal, */
        getContentPane();
        panelContenedor = new JPanel(); 
        panelContenedor.setLayout(new BorderLayout());
        panelContenedor.setBorder(javax.swing.BorderFactory.createTitledBorder("BOOM!"));
        panelContenedor.setBackground(Color.getHSBColor(102, 153,44));
        
        panelJugador = new JPanel();
        tanque = new JLabel();
        tanque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/10988937_922865367757522_1294908873400453276_n.jpg"))); 
        panelJugador.add(tanque);
        
        panelLevels = new JPanel(new GridBagLayout());
        gbc = new GridBagConstraints();
        panelLevels.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        panelLevels.setBackground(Color.RED);
        level1 = new JButton("LEVEL 1");
        level2 = new JButton("LEVEL 2");
        level3 = new JButton("LEVEL 3");
        gbc.gridx =0; 
        gbc.gridy =0; 
        panelLevels.add(level1,gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        panelLevels.add(level2,gbc);
        gbc.gridx = 0;
        gbc.gridy = 2; 
        panelLevels.add(level3,gbc);
        
        panelBotones = new JPanel();
        panelBotones.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        exit = new JButton("EXIT");
        start = new JButton("START");
        start.setBackground(Color.LIGHT_GRAY);
        exit.setBackground(Color.LIGHT_GRAY);
        panelBotones.add(exit);
        panelBotones.add(start);
        
       // panelLabels = new JPanel(new GridLayout(1,2,20,20));
        //panelLabels.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        //puntuacion = new JLabel("Puntuacion: " + " 0");
        //vida = new JLabel("Vida: " + " 100");
        //panelLabels.add(puntuacion);
        //panelLabels.add(vida);
        
        
       // panelContenedor.add(panelLabels, BorderLayout.NORTH);
        panelContenedor.add(panelJugador, BorderLayout.CENTER);
        panelContenedor.add(panelLevels, BorderLayout.WEST);
        panelContenedor.add(panelBotones, BorderLayout.SOUTH);
        this.add(panelContenedor);
        pack();
    }
   
    }
    
  
